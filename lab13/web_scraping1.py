"""Volume 3B: Web Scraping 1. Spec file."""
from bs4 import BeautifulSoup
import re
import datetime

html_doc = """
    <html><head><title>The Three Stooges</title></head>
    <body>
    <p class="title"><b>The Three Stooges</b></p>
    <p class="story">Have you ever met the three stooges? Their names are
    <a href="http://example.com/larry" class="stooge" id="link1">Larry</a>,
    <a href="http://example.com/mo" class="stooge" id="link2">Mo</a> and
    <a href="http://example.com/curly" class="stooge" id="link3">Curly</a>;
    and they are really hilarious.</p>
    <p class="story">...</p>
    """

# Problem 1
def prob1(filename):
    """
    Find all of the tags used in a particular .html file and the value of
    the 'type' attribute.

    Inputs:
        filename: Name of .html file to parse

    Outputs:
        - A set of all of the tags used in the .html file
        - The value of the 'type' attributes for the style tag
    """
    example = open(filename).read()
    soup = BeautifulSoup(example)
    tags = soup.find_all()
    tags_used = set()
    for tag in tags:
        tags_used.add(tag.name)
    value = soup.find('style')['type']
    return tags_used, value

# Problem 2
def prob2():
    """Prints (not returns) the prettified
    string for the Three Stooges HTML.
    """
    soup = BeautifulSoup(html_doc, 'html.parser')
    print soup.prettify()

# Problem 3
def prob3():
    """Returns [u'title'] from the Three Stooges soup"""
    soup = BeautifulSoup(html_doc, 'html.parser')
    return soup.title.text

# Problem 4
def prob4():
    """Returns u'Mo' from the Three Stooges soup"""
    soup = BeautifulSoup(html_doc, 'html.parser')
    return soup.a.next_sibling.next_sibling.text
    

# Problem 5
def prob5(method):
    """Returns the u'More information...' using two different methods.
    If method is 1, it uses first method. If method is 2, it uses
    the second method.
    """
    soup = BeautifulSoup(open('example.html'), 'html.parser')
    if method == 1:
        return soup.div.p.next_sibling.next_sibling.a.text
    if method == 2:
        return soup.div.a.text

# Problem 6
def prob6(method):
    """Returns the tag associated with the "More information..."
    link using two different methods. If method is 1, it uses the
    first method. If method is 2, it uses the second method.
    """
    soup = BeautifulSoup(open('example.html'), 'html.parser')
    if method == 1:
        return soup.find('a')
    if method == 2:
        return soup.find('a',href='http://www.iana.org/domains/example')

# Problem 7
def prob7():
    """Loads 'SanDiegoWeather.htm' into BeautifulSoup and prints
    (not returns) the tags referred to the in the Problem 7 questions.
    """
    soup = BeautifulSoup(open('SanDiegoWeather.html'), 'html.parser')
    soup.prettify()
    # Question 1
    print soup.find('h2', class_="history-date")
    # Question 2
    print soup.find('div', class_="previous-link")
    print soup.find('div', class_="next-link")
    # Question 3
    print soup.find('td', text="Max Temperature").parent.find_all('span')[2]


# Problem 8
def prob8():
    """Loads 'Big Data dates.htm' into BeautifulSoup and uses find_all()
    and re to return a list of all tags containing links to bank data
    from September 30, 2003 to December 31, 2014.
    """
    soup = BeautifulSoup(open('Big-Data-dates.html'), 'html.parser')
    links = soup.find_all(href=re.compile('releases/lbr'))
    good_links = []
    for link in links:
        if re.match('.*, ', link.text):
            date = datetime.datetime.strptime(link.text, '%B %d, %Y')
            if datetime.datetime(2003, 9, 30) <= date <= datetime.datetime(2014, 12, 31):
                good_links.append(link)
    return good_links

if __name__ == '__main__':
    print prob8()
