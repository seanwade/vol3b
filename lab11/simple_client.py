#!/usr/bin/env python

# Simple Client
# Sean Wade

from __future__ import print_function, division
import time
import socket

IP = '0.0.0.0'
PORT = 8001
SIZE = 2048

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((IP, PORT))

msg = "This is a test message."
client.send(msg)

print("Waiting for the server to send back data...")
data = client.recv(SIZE)
print("Server echoed back: %s" % data)

client.close()
