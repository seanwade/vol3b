#!/usr/bin/env python

# Simple Server
# Sean Wade

from __future__ import print_function, division
import time
import random
import socket

ADDRESS = '0.0.0.0'
PORT = 8001
SIZE = 2048

MOVE_LIST = ['rock', 'paper', 'scissors']

print("...Starting game server...")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((ADDRESS, PORT))
s.listen(1)

conn, addr = s.accept()
print('Game joined by: ', addr)

while True:
    serverMove = random.choice(MOVE_LIST)
    clientMove = conn.recv(SIZE)
    print("User played: ", clientMove)
    print("Server played: ", serverMove)
    if not clientMove:
        break

    if clientMove == 'rock':
        if serverMove == 'rock':
            result = 'draw'
        if serverMove == 'paper':
            result = 'you lose'
        if serverMove == 'scissors':
            result = 'you win'
    elif clientMove == 'paper':
        if serverMove == 'rock':
            result = 'you win'
        if serverMove == 'paper':
            result = 'draw'
        if serverMove == 'scissors':
            result = 'you lose'
    elif clientMove == 'scissors':
        if serverMove == 'rock':
            result = 'you lose'
        if serverMove == 'paper':
            result = 'you win'
        if serverMove == 'scissors':
            result = 'draw'
    else:
        print(clientMove, " : is invalid.")
        result = 'invalid move'

    conn.send(result)

conn.close()
