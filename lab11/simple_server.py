#!/usr/bin/env python

# Simple Server
# Sean Wade

from __future__ import print_function, division
import time
import random
import socket

ADDRESS = '0.0.0.0'
PORT = 8001
SIZE = 2048


print("Starting server...")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((ADDRESS, PORT))
s.listen(1)

conn, addr = s.accept()

print("Accepting connection from:", addr)
while True:
    data = conn.recv(SIZE)
    if not data:
        break
    data = data + time.strftime('%H:%M:%S')
    conn.send(data)

conn.close()
