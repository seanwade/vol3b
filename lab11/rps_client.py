#!/usr/bin/env python

# Rock, paper, scissors client
# Sean Wade

from __future__ import print_function, division
import time
import socket

IP = '0.0.0.0'
PORT = 8001
SIZE = 2048

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((IP, PORT))
print("...Starting game...")


while True:
    clientMove = raw_input("Choose your move: ")
    print('clientMove: ', clientMove)
    client.send(clientMove)
    response = client.recv(SIZE)
    print("Match Results: ", response)
    if response == 'you win':
        break

client.close()
