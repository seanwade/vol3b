# nameserver.py
"""Vol 3B: Web Tech 1 (Internet Protocols). Auxiliary file.
Sean Wade
"""

from __future__ import division, print_function
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import requests
import urlparse
import os

names = {   "Babbage": "Charles",
            "Berners-Lee": "Tim",
            "Boole": "George",
            "Cerf":"Vint",
            "Dijkstra":"Edsger",
            "Hopper":"Grace",
            "Knuth":"Donald",
            "von Neumann":"John",
            "Russel":"Betrand",
            "Shannon":"Claude",
            "Turing":"Alan"        }

startsWith = {}
allNames = []
for key, val in names.iteritems():
    letter = key[0]
    if letter not in startsWith:
        startsWith[letter] = []
    fullName = key + ', ' + val
    allNames.append(fullName)
    startsWith[letter].append(fullName)

class NameServerHTTPRequestHandler(BaseHTTPRequestHandler):
    """Custom HTTPRequestHandler class"""

    def do_GET(self):
        """Handle GET command"""
        self.send_response(200)

        parsed_path = urlparse.urlparse(self.path)
        try:
            params = dict([p.split('=') for p in parsed_path[4].split('&')])
        except:
            params = {}

        # Send header first
        self.send_header("Content-type","text-html")
        self.end_headers()

        # Send content to client
        try:
            if params["lastname"] == 'AllNames':
                response = '\n'.join(allNames)
            elif params["lastname"] in names:
                response = names[params["lastname"]]
            elif params["lastname"] in startsWith:
                response =  '\n'.join(startsWith[params["lastname"]])
            else:
                response = "Not valid query."

            self.wfile.write(response)
        except:
            pass
        return

    def do_PUT(self):
        self.send_response(200)
        content_len = int(self.headers.getheader('content-length', 0))
        post_body = self.rfile.read(content_len)

        try:
            params = dict([p.split('=') for p in post_body.split('&')])
        except:
            params = {}

        names[params['lastname']] = params['firstname']
        fullName = params['lastname'] + ', ' + params['firstname']
        allNames.append(fullName)
        startsWith[letter].append(fullName)
        




def run():
    print("http server is starting...")
    server_address = ("0.0.0.0", 8000)
    httpd = HTTPServer(server_address, NameServerHTTPRequestHandler)
    print("http server is running...")
    httpd.serve_forever()

if __name__ == '__main__':
    run()
