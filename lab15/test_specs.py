# test_specs.py
"""Python Essentials: Testing.
Sean Wade
"""

import specs
import pytest
import numpy as np

# Problem 1: Test the addition and smallest factor functions from specs.py
def test_addition():
    assert specs.addition(1, 2) == 3

def test_smallest_factor():
    assert specs.smallest_factor(1) == 1
    assert specs.smallest_factor(6) == 2
    assert specs.smallest_factor(3) == 3

# Problem 2: Test the operator function from specs.py
def test_operator():
    with pytest.raises(Exception) as excinfo:
      specs.operator(5, 5, 5)
    print excinfo
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Oper should be a string"

    with pytest.raises(Exception) as excinfo:
      specs.operator(5, 5, '+-')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Oper should be one character"

    with pytest.raises(Exception) as excinfo:
      specs.operator(5, 0, '/')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "You can't divide by zero!"

    assert specs.operator(5, 1, '/') == 5/1.
    assert specs.operator(5, 1, '-') == 4
    assert specs.operator(5, 1, '+') == 6
    assert specs.operator(5, 1, '*') == 5

    with pytest.raises(Exception) as excinfo:
      specs.operator(5, 0, 'a')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Oper can only be: '+', '/', '-', or '*'"

# Problem 3: Finish testing the complex number class
@pytest.fixture
def set_up_complex_nums():
    number_1 = specs.ComplexNumber(1, 2)
    number_2 = specs.ComplexNumber(5, 5)
    number_3 = specs.ComplexNumber(2, 9)
    return number_1, number_2, number_3

def test_complex_addition(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1 + number_2 == specs.ComplexNumber(6, 7)
    assert number_1 + number_3 == specs.ComplexNumber(3, 11)
    assert number_2 + number_3 == specs.ComplexNumber(7, 14)
    assert number_3 + number_3 == specs.ComplexNumber(4, 18)

def test_complex_subtraction(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1 - number_2 == specs.ComplexNumber(-4, -3)
    assert number_1 - number_3 == specs.ComplexNumber(-1, -7)
    assert number_2 - number_3 == specs.ComplexNumber(3, -4)
    assert number_3 - number_3 == specs.ComplexNumber(0, 0)

def test_complex_multiplication(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1 * number_2 == specs.ComplexNumber(-5, 15)
    assert number_1 * number_3 == specs.ComplexNumber(-16, 13)
    assert number_2 * number_3 == specs.ComplexNumber(-35, 55)
    assert number_3 * number_3 == specs.ComplexNumber(-77, 36)

def test_complex_norm(set_up_complex_nums):
  number_1, number_2, number_3 = set_up_complex_nums
  assert number_1.norm() == np.sqrt(5)

def test_complex_division(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_3 / number_3 == specs.ComplexNumber(1, 0)
    with pytest.raises(Exception) as excinfo:
      number_3 / specs.ComplexNumber(0, 0)
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Cannot divide by zero"

def test_complex_print(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert str(number_1) == "1+2i"

# Problem 4: Write test cases for the Set game.

def set_up_file_helper(numpy_array, hard_save=None):
  np.savetxt('test_set.txt', numpy_array, delimiter=',', fmt='%i')
  if hard_save:
    np.savetxt(hard_save, numpy_array, delimiter=',', fmt='%i')

def test_set_input():
  wrong_size_set = np.array([[2, 2, 2, 2]]*11)
  set_up_file_helper(wrong_size_set, './hands/wrong_size_set.txt')

  with pytest.raises(Exception) as excinfo:
    specs.solve_set('test_set.txt')
  assert excinfo.typename == 'ValueError'
  assert excinfo.value.args[0] == "You only provided 11 cards. I need 12."

  wrong_set_num = np.array([[2, 2, 2, 3]]*12)
  set_up_file_helper(wrong_set_num, './hands/wrong_size_num.txt')

  with pytest.raises(Exception) as excinfo:
    specs.solve_set('test_set.txt')
  assert excinfo.typename == 'ValueError'
  assert excinfo.value.args[0] == "And atribute was a 3. I expect 0, 1, and 2 as attributes"

  wrong_set_num = np.array([[2, 2, 2]]*12)
  set_up_file_helper(wrong_set_num, './hands/wrong_size_attrs.txt')

  with pytest.raises(Exception) as excinfo:
    specs.solve_set('test_set.txt')
  assert excinfo.typename == 'ValueError'
  assert excinfo.value.args[0] == "Each card has 4 attributes not 3"

  answer = specs.solve_set('./hands/real_set.txt')
  expected = [np.array([[ 1.,  1.,  2.,  1.],
       [ 0.,  0.,  1.,  2.],
       [ 2.,  2.,  0.,  0.]]), np.array([[ 0.,  0.,  0.,  0.],
       [ 0.,  0.,  0.,  1.],
       [ 0.,  0.,  0.,  2.]]), np.array([[ 0.,  0.,  0.,  0.],
       [ 1.,  1.,  1.,  1.],
       [ 2.,  2.,  2.,  2.]]), np.array([[ 0.,  0.,  0.,  1.],
       [ 1.,  1.,  1.,  0.],
       [ 2.,  2.,  2.,  2.]]), np.array([[ 0.,  0.,  0.,  2.],
       [ 1.,  1.,  1.,  2.],
       [ 2.,  2.,  2.,  2.]]), np.array([[ 1.,  1.,  1.,  1.],
       [ 1.,  1.,  1.,  2.],
       [ 1.,  1.,  1.,  0.]])]
  assert_list = [np.all(answer[i] == expected[i]) for i in range(len(answer))]
  assert all(assert_list) == True





