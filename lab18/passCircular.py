from mpi4py import MPI
from mpi4py.MPI import ANY_SOURCE
from sys import argv
import numpy as np

"""
Write a script that runs on n processes and passes an n by 1 array
of random values from the ith process to the i+1th process (or to the 0th
process for the if i is the last process).  Have each process identify
itself and what it is doing:

    e.g. "Process {process_num} sending array {array to process_num + 1}"
    and "Process {process_num} just received {array from process_num - 1}"
"""
n = int(argv[1])

RANK = MPI.COMM_WORLD.Get_rank()
SIZE = MPI.COMM_WORLD.Get_size()

a = np.random.rand(n)
b = np.zeros(n)

dest = (RANK + 1) % SIZE

print "Process {} sending {} to Process {}".format(RANK, a, dest)
MPI.COMM_WORLD.Send(a, dest=dest)
MPI.COMM_WORLD.Recv(a, source=ANY_SOURCE)
print 'Process {} received {}'.format(RANK, b)
