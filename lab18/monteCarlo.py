from mpi4py import MPI
from sys import argv
import numpy as np

"""
Write a script that runs a Monte Carlo simulation to approximate the volume
of an m-dimensional unit sphere.  Your script should accept the following
parameters as command line arguments:
    n - number of processes (passed as normal into mpirun)
    m - dimension of unit sphere
    p - number of points to use in simulation

For your final answer, print out the approximated volume from your simulation.
"""
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()
SIZE = COMM.Get_size()

m, p = int(argv[1]), int(argv[2])

points = np.random.rand(p, m)
norms = np.linalg.norm(points, axis=1)
points_in_sphere = sum(norms < 1)

local_vol = 2**m*points_in_sphere/p

vol = COMM.gather(local_vol, root=0)

if RANK == 0:
    vol = np.mean(np.array(vol))
    print 'The Volume of a %d-sphere is %f' % (m, vol)
