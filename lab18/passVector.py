from mpi4py import MPI
from sys import argv
import numpy as np


"""
Write a script that runs on two processes and passes an n by 1 array
of random values from one process to the other.  Have each process identify
itself and what it is doing:

    e.g. "Process 1 sending array {array}"
    and "Process 0 just received {array}"
"""
COMM = MPI.COMM_WORLD
RANK = MPI.COMM_WORLD.Get_rank()

n = int(argv[1])
if RANK == 1:
    send_vec = np.random.rand(n)
    print('Process 1: Sending {}'.format(send_vec))
    COMM.Send(send_vec,dest=0)

if RANK == 0:
    rec_vec = np.zeros(n)
    COMM.Recv(rec_vec, source=1)
    print('Process 0: Received {}'.format(rec_vec))
