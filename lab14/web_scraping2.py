"""Volume III: Web Scraping 2.
Sean Wade
"""

from bs4 import BeautifulSoup
import urllib2
import pandas as pd
import re
import numpy as np
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import sys
import codecs

#sys.stdout = codecs.getwriter('utf8')(sys.stdout)

# Problem 1
def Prob1():
    """Load the Big Bank Info file and return a pandas DataFrame
    containing Bank Name, Rank, ID, Domestic Assets, and Domestic Branches
    for JP Morgan, Capital One, and Discover banks.
    """
    site = open('federalreserve.htm').read()
    soup = BeautifulSoup(site, 'html.parser')

    columns = ['Bank_Name','Rank','ID','Domestic_Assets','Num_Domestic_Branches']
    bank_list = ['JPMORGAN','CAPITAL ONE','DISCOVER']
    col_index = [0,1,2,6,9]
    data = []
   
    table = soup.find('table').find('tbody')
    rows = table.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        if any(bank in cols[0].text for bank in bank_list): 
            data.append([cols[i].text for i in col_index])
    return pd.DataFrame(data, columns=columns)
    

# Problem 2
def Prob2(url=None):
    """Use urllib2 and BeautifulSoup to return the actual max temperature,
    the tag containing the link for 'Next Day', and the url associated
    with that link.
    """
    if not url:
        url = 'https://math.byu.edu/weather/www.wunderground.com/history/airport/KSLC/2015/1/1/DailyHistory.html?req_city=&req_state=&req_statename=&reqdb.zip=&reqdb.magic=&reqdb.wmo='
    web_content = urllib2.urlopen(url).read()
    soup = BeautifulSoup(web_content, 'html.parser')
    max_temp = soup.find_all('span',class_='wx-data')[2].text
    next_day_link = soup.find('div', class_='next-link')
    return max_temp, next_day_link, next_day_link.next['href']

# Problem 3
def Prob3():
    """Mimic the Wunderground Weather example to create a list of average
    max temperatures of the year 2014 in San Diego. Use matplotlib to draw
    a graph depicting the data, then return the list.
    """
    link = 'https://math.byu.edu/weather/www.wunderground.com/history/airport/KSLC/2015/1/1/DailyHistory.html?req_city=&req_state=&req_statename=&reqdb.zip=&reqdb.magic=&reqdb.wmo='
    content = urllib2.urlopen(link).read()
    soup = BeautifulSoup(content, 'html.parser')
    data = []
    base = 'https://math.byu.edu/weather/www.wunderground.com/history/airport/KSLC/2015/'
    month = 'jan'

    while not 'March' in soup.find('h2', class_='history-date').text:
        max_temp, next_day_link, link = Prob2(link)
        print soup.find('h2', class_='history-date').text
        print "Adding: %s" % max_temp
        data.append(int(max_temp[:-2]))
        if link[:4] == '../.':
            month = 'feb'
            link = '/2/' + link[2:]
            print link
        if month == 'jan':
            link = base + '1' + link[2:]
        else:
            link = base + '2' + link[2:]

        content = urllib2.urlopen(link).read()
        soup = BeautifulSoup(content, 'html.parser')

    return data


# Problem 4
def Prob4():
    """Load the selected option into BeautifulSoup. Find the requested
    information for the selected option and make a SQL table that
    stores this information.
    """
    url = 'http://google.com/finance'
    content = urllib2.urlopen(url).read()
    soup = BeautifulSoup(content)
    summary_list = soup.find('div', id='country-widget').find_all('span')
    links = [summary_list[x].parent.parent.find('a')['href'] for x in range(len(summary_list))]

    base = 'http://google.com'
    data = []
    for link in links:
        try:
            content = urllib2.urlopen(base + link)
            link_soup = BeautifulSoup(content)
            change_list = link_soup.find('b', text='Gainers (% price change)').parent.parent.parent.find_all('span', class_='chg')
            changes = change_list[:10:2]
            changes = [float(x.text[1:]) for x in changes]
            best_idx = changes.index(max(changes))
            row = change_list[best_idx].parent.parent
            name = row.find_all('a')[0].text
            abbr = row.find_all('a')[1].text
            per_chng = row.find_all('span', class_='chg')[1].text
            mkt_cap = row.find_all('td')[-1].text.strip()
            data.append([name, abbr, per_chng, mkt_cap])
        except:
            print "Link (%s) has no gainers..." % link
    data = pd.DataFrame(data=data, columns=['Name', 'Abreviation', 'perc Change', 'MKT Cap'])
    # data.to_sql('problem_5_database.db') to convert to SQL
    return data

# Problem 5
def Prob5():
    """Use selenium to return a list of all the a tags containing each of the
    30 NBA teams. Return only one tag per team.
    """
    driver = webdriver.Chrome('/Users/seanwade/projects/acme/vol3b/lab14//chromedriver')
    url = 'http://stats.nba.com/league/team/#!/?sort=W&dir=1'
    driver.get(url)
    soup = BeautifulSoup(driver.page_source)
    teams = soup.find('div', split='splits.TeamID').find_all('option')
    return teams


if __name__ == '__main__':
    print Prob1()
