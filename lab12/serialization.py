#serialization.py
'''Volume 3B: Web Technologies 2 - Serialization
Sean Wade
Volume 3b
'''

import json
import xml
import  xml.etree.ElementTree as et
import datetime
import requests
from datetime import datetime
from scipy.spatial import cKDTree

# Problem 1
class DateEncoder(json.JSONEncoder):
    '''
    This is a class shell for the datetime object
    encoder. See example in Lab PDF for more
    information.
    '''
    pass

def date_decoder(dct):
    '''
    This method should be used to decode a
    datetime object from JSON format to Python.
    See Lab PDF for more information.
    '''
    def dedate(s):
        # Put information here.
        pass
    
    # Try to decode any value that looks like a date
    for i, k in dct.iteritems():
        try:
            dct[i] = dedate(k)
        except:
            continue
    return dct
    
# Problem 2
def water_data():
    '''
    This method should get a JSON file from 
    https://data.lacity.org/resource/v87k-wgde.json
    using the requests library. It should then
    show a scatter plot of the water usage from
    2012 to 2013 organized by latitude and longitude.
    
    Scatter Plot:
    x-axis: longitude
    y-axis: latitude
    point size: water use (hundreds of cubic feet)
    '''
    print requests.get("https://data.lacity.org/resource/v87k-wgde.json").json()
 
    from bokeh.plotting import figure
    from bokeh.models import WMTSTileSource
    fig = figure(title="Los Angeles Water Usage 2012-2013", plot_width=600,
	plot_height=600, tools=["wheel_zoom", "pan", "hover"],
		x_range=(-13209490, -13155375), y_range=(3992960, 4069860),
		webgl=True, active_scroll="wheel_zoom")
    fig.axis.visible = False

    STAMEN_TONER_BACKGROUND = 
    
# Problem 3
def books_xml():
    '''
    This method should include all code used to 
    find:
    - The author of the most expensive book
    - How many books were published before May 1,
    2000
    - Which books reference Microsoft in their 
    description
    
    in books.xml
    ''' 
    f = et.parse('books.xml')
    catalog = f.getroot() 

    best_price = 0
    book_cnt = 0
    cutoff_date = datetime.strptime('2000-05-1', '%Y-%m-%d')
    microsoft_books = []

    for book in catalog.iter('book'):
        author = book.find('author').text
        title = book.find('title').text
        price = book.find('price').text
        date_text = book.find('publish_date').text
        date = datetime.strptime(date_text, '%Y-%m-%d')

        description = book.find('description').text
        
        if price > best_price:
            most_epensive_book = title 
            best_price = price

        if date < cutoff_date:
            book_cnt += 1

        if "Microsoft" in description:
            microsoft_books.append(title)

    print "The most expensive book is: %s" % most_epensive_book
    print "There are %d books published before 5/1/2000" % book_cnt
    print "Books that reference Micorsoft: ", microsoft_books



# Problem 4
def recycle_bins():
    '''
    This method should automatically pull the XML
    file from https://data.cityofnewyork.us/api/
    views/sxx4-xhzg/rows.xml?accessType=DOWNLOAD.
    It should then calculate and return the average
    distance between each bin and its closest 
    neighbor.
    '''
    # Get the data
    data_txt = requests.get("https://data.cityofnewyork.us/api/views/sxx4-xhzg/rows.xml?accessType=DOWNLOAD").content
    tree = et.fromstring(data_txt)
    for trash in list(tree):
        trash.find('latitude')

water_data()

