import json
import datetime
import requests
import pandas as pd
import numpy as np
import xml.etree.ElementTree as et

from pyproj import Proj, transform
from scipy.spatial import cKDTree

# Problem 1
class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
	if isinstance(obj, datetime.datetime): return {'dtype': 'datetime',
		'data': str(obj)}
	return json.JSONEncoder.default(self, obj)

def DateTimeDecoder(item):
    accepted_dtypes = {'datetime':datetime.datetime}
    ty = accepted_dtypes.get(item['dtype'], None)
    if ty is not None and 'data' in item:
        return datetime.datetime.strptime(item['data'], '%Y-%m-%d %H:%M:%S.%f')
    return item

# Problem 2
def prob2():
    print "This is in water_bokeh.py"

# Problem 3
def prob3():
    f = et.parse('books.xml')
    catalog = f.getroot() 

    best_price = 0
    book_cnt = 0
    cutoff_date = datetime.datetime.strptime('2000-12-1', '%Y-%m-%d')
    microsoft_books = []

    for book in catalog.iter('book'):
        author = book.find('author').text
        title = book.find('title').text
        price = float(book.find('price').text)
        date_text = book.find('publish_date').text
        date = datetime.datetime.strptime(date_text, '%Y-%m-%d')

        description = book.find('description').text
        
        if price > best_price:
            most_epensive_book = author 
            best_price = price

        if date < cutoff_date:
            book_cnt += 1

        if "Microsoft" in description:
            microsoft_books.append(title)

    print "The author with the most expensive book is", most_epensive_book
    print "The number of books published before Dec 1, 2000 is", book_cnt
    print "The books that reference Microsoft in their description are", microsoft_books


# Problem 4
def convert(longitudes, latitudes):
    from_proj = Proj(init="epsg:4326")
    to_proj = Proj(init="epsg:3857")

    x_vals = []
    y_vals = []
    for lon, lat in zip(longitudes, latitudes):
        x, y = transform(from_proj, to_proj, lon, lat)
        x_vals.append(x)
        y_vals.append(y)

    return x_vals, y_vals

def prob4():
    pass


def test1():
    d = datetime.datetime.now()
    ser = json.dumps(d, cls=DateTimeEncoder)
    d = json.loads(ser, object_hook=DateTimeDecoder)
    print d


if __name__ == "__main__":
    prob3()


