# Sean Wade

from bokeh.plotting import figure, show
from bokeh.models import WMTSTileSource, ColumnDataSource
from pyproj import Proj, transform
import requests
import numpy as np
from bokeh.io import curdoc
from bokeh.layouts import column, row, layout, widgetbox
import json

from_proj = Proj(init="epsg:4326")
to_proj = Proj(init="epsg:3857")

def convert(longitudes, latitudes):
    lon, lat = longitudes, latitudes
    x, y = transform(from_proj, to_proj, lon, lat)
    return x, y

fig = figure(title="Los Angeles Water Usage 2012-2013", plot_width=600,
                plot_height=600, tools=["wheel_zoom", "pan"],
                x_range=(-13209490, -13155375), y_range=(3992960, 4069860),
                webgl=True, active_scroll="wheel_zoom")
fig.axis.visible = False
STAMEN_TONER_BACKGROUND = WMTSTileSource(
    url='http://tile.stamen.com/toner-background/{Z}/{X}/{Y}.png',
    attribution=(
        'Map tiles by <a href="http://stamen.com">Stamen Design</a>, '
        'under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>.'
        'Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, '
        'under <a href="http://www.openstreetmap.org/copyright">ODbL</a>'
    )
)
background = fig.add_tile(STAMEN_TONER_BACKGROUND)

# Get the data
data = requests.get("https://data.lacity.org/resource/v87k-wgde.json").json()
lats = []
lons = []
water_use = []
for use in data:
    water_use.append(use["fy_12_13"])
    lat, lon = use['location_1']['coordinates']
    lat, lon = convert(lat, lon)
    lats.append(lat)
    lons.append(lon)

source = ColumnDataSource(dict(
    x = lats,
    y = lons,
    water_use = water_use))

fig.circle('x', 'y', source=source, fill_color="red", size='water_use')
show(fig)
