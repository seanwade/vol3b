import requests
from bs4 import BeautifulSoup
import urllib2

link = 'https://www.ultimate-guitar.com/top/top100.htm'

opener = urllib2.build_opener()
opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
content = opener.open(link).read()

soup = BeautifulSoup(content, 'html.parser')

print soup.find_all('table',cellspacing="0",cellpadding="2",width="100%",border="0")
