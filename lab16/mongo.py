"""
Sean Wade
MongoDB
"""

from pymongo import MongoClient
import json
import re


# Set up the client
mc = MongoClient(port = 12345)
mydb = mc.db1
rest = mydb.collection1

def prob1():

    for i in open('restaurants.json'):
        rest.insert(json.loads(i))

def prob2():
    
    for i in open('mylans_bistro.json'):
	rest.insert(json.loads(i))
    r = rest.find({'closing_time':'1800'})
    return [j['name'] for j in r]

def prob3():
    
    manhattan = rest.find({"borough": "Manhattan"}).count()
    grades = rest.find({'grades.grade': 'A'}).count()
    northernmost = list(rest.find({"address.coord.1": {'$gt': 45}}))
    northernmost_list = [place['name'] for place in northernmost]
    grills = list(rest.find({'name': {'$regex': ".*[Gg][Rr][Ii][Ll][Ll].*"}}))
    grills_list = [place['name'] for place in grills]

    return (manhattan, grades, northernmost_list, grills_list)

def prob4():

    grills = list(rest.find({'name': {'$regex': ".*[Gg][Rr][Ii][Ll][Ll].*"}}))
    restaurant_name_list = [place['name'] for place in grills]
    for name in restaurant_name_list:
    	rest.update_one({'name': name}, {'$set': {'name' : name.replace('Grill', "Magical Fire Table")}})
    ids = [i['restaurant_id'] for i in rest.find()]
    for idk in ids:
        rest.update_one({'restaurant_id': idk}, {'$set' : {'restaurant_id' : str(int(idk)+1000)}})
    got_milk = [i['restaurant_id'] for i in rest.find({'grades.grade': 'C'})]
    for go in got_milk:
        rest.remove({'restaurant_id': go})

